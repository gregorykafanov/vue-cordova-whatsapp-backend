const express = require('express')
const app = express()
const socket = require('socket.io')
const { v4: uuidv4 } = require('uuid')
const util = require('util')
const port = 3000

app.use(express.urlencoded())
app.use(express.json())

let state = {
  usersDB: {
    data: {
      1: {
        avatar: 'http://localhost:8080/images/avatars/AnnetStark.png',
        name: 'Annet Stark',
        password: '@annetstark',
        email: 'annet.s@filancy.com',
        chatRoomsIds: ['1', '2', '5'],
      },
      2: {
        avatar: 'http://localhost:8080/images/avatars/1112.png',
        name: 'Jhon Doe',
        password: '@johndoe',
        email: 'john.d@filancy.com',
        chatRoomsIds: ['5'],
      },
      3: {
        avatar: 'http://localhost:8080/images/avatars/1111.png',
        name: 'Merry Wix',
        password: '@merrywix',
        email: 'merry.w@filancy.com',
        chatRoomsIds: ['1'],
      },
      4: {
        avatar: 'http://localhost:8080/images/avatars/1113.png',
        name: 'Mike Berman',
        password: '@mikeberman',
        email: 'mike.b@filancy.com',
        chatRoomsIds: ['2'],
      },
    },
    ids: ['1', '2', '3', '4'],
  },
  chatsDB: {
    data: {
      1: {
        ids: ['1', '3'],
        dialog: [
          { id: '1', userId: '1', date: '23.02.2021', message: 'Hi!' },
          { id: '2', userId: '3', date: '23.02.2021', message: 'Whats up!' },
          {
            id: '3',
            userId: '1',
            date: '23.02.2021',
            message: `Let's go to the park!`,
          },
          {
            id: '4',
            userId: '3',
            date: '23.02.2021',
            message: `Ok. I'm on my way.`,
          },
        ],
      },
      2: {
        ids: ['1', '4'],
        dialog: [
          {
            id: '5',
            userId: '1',
            date: '23.02.2021',
            message: 'I say BeBeBe!',
          },
          {
            id: '6',
            userId: '4',
            date: '23.02.2021',
            message: 'So get from me MeMeMe',
          },
          {
            id: '7',
            userId: '1',
            date: '23.02.2021',
            message: `We have very philosofical conversation`,
          },
          {
            id: '8',
            userId: '4',
            date: '23.02.2021',
            message: `Yes, it means we are exist`,
          },
        ],
      },
      5: {
        ids: ['1', '2'],
        dialog: [
          {
            id: '5',
            userId: '1',
            date: '23.02.2021',
            message: 'I say BeBeBe!',
          },
          {
            id: '6',
            userId: '2',
            date: '23.02.2021',
            message: 'So get from me MeMeMe',
          },
          {
            id: '7',
            userId: '1',
            date: '23.02.2021',
            message: `We have very philosofical conversation`,
          },
          {
            id: '8',
            userId: '2',
            date: '23.02.2021',
            message: `Konichiua`,
          },
          {
            id: '9',
            userId: '2',
            date: '23.02.2021',
            message: `Uno`,
          },
        ],
      },
    },
    ids: ['1', '2', '5'],
  },
}

app.get('/', (req, res) => {
  res.json(state)
})

app.post('/', (req, res) => {
  res.json(state)
})

// CHATS ------------------
app.get('/chats', (req, res) => {
  res.json(state.chatsDB.data)
})

app.post('/chats/:id', (req, res) => {
  res.json(state.chatsDB.data[req.params.id])
})

// USERS ------------------
app.get('/users', (req, res) => {
  res.json(state.usersDB.data)
})

app.post('/users/:id', (req, res) => {
  res.json(state.usersDB.data[req.params.id])
})

// DIALOG -----------------
app.post('/dialogmsg', (req, res) => {
  const reqBody = req.body

  state.chatsDB.data[reqBody.chatId].dialog.push({
    id: uuidv4(),
    userId: reqBody.userId,
    date: '--date--',
    message: reqBody.message,
  })
  res.json({ success: true, message: 'added' })
  io.sockets.emit('DIALOG_UPDATE', state.chatsDB.data[reqBody.chatId].dialog)
})

// SERVER SOCKETS ------------------
const server = app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
})

const io = socket(server)

io.on('connection', socket => {
  console.log('websockets connected to ' + socket.id)
  socket.on('NEW_MESSAGE', msg => {
    const { chatId, userId, message } = msg
    state.chatsDB.data[chatId].dialog.push({
      id: uuidv4(),
      userId: userId,
      date: '--date--',
      message: message,
    })
    io.sockets.emit('DIALOG_UPDATE', state.chatsDB.data[chatId].dialog)
  })
  socket.on('INIT_DIALOG', chatId => {
    io.sockets.emit('DIALOG_UPDATE', state.chatsDB.data[chatId.chatId].dialog)
  })
  socket.on('ON_LOGIN', crdentials => {
    if (
      crdentials.email === 'annet.s@filancy.com' &&
      crdentials.password === '@annetstark'
    ) {
      io.sockets.emit('ON_USER_ID', 1)
    }
    if (
      crdentials.email === 'john.d@filancy.com' &&
      crdentials.password === '@johndoe'
    ) {
      io.sockets.emit('ON_USER_ID', 2)
    }
  })
})
